export enum Icons {
    GAMEPAD='gamepad',
    JAR='jar',
    BOOK='book',
    CLOUD='cloud',
    CHART='chart-column',
    BOWL='bowl-food'
}
