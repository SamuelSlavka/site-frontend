export enum BackgroundColors {
    DARK="#264653",
    MIDDLE="#2a9d8f",
    LIGHT="#e9c46a",
    ORANGE="#f4a261",
    RED="#e76f51",
    BLUE="#1c478c"
}
